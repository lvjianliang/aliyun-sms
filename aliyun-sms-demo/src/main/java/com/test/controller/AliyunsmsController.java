package com.test.controller;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.aliyuncs.exceptions.ClientException;

import cn.javaer.aliyun.sms.SmsClient;
import cn.javaer.aliyun.sms.SmsTemplate;
import cn.javaer.aliyun.spring.boot.autoconfigure.sms.SmsProperties;

@RestController
public class AliyunsmsController {

	@Autowired
	private SmsClient smsClient;
	
	@Autowired
	SmsProperties properties;
	
	@RequestMapping(value="/send/{phone}",method=RequestMethod.GET)
	public void send(@PathVariable String phone) {
		 List phones=Arrays.asList(phone.split(","));//电话数组
		 
		SmsTemplate smsTemplate = SmsTemplate.builder()
	            .signName(properties.getSignName())//签名
	            .templateCode(properties.getTemplates().get("key1").getTemplateCode())//选择模板code
	            .addTemplateParam("code", "123456")//添加参数
	            .phoneNumbers(phones)
	            
	            .build();
	try {
		smsClient.send(smsTemplate);
	} catch (ClientException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}    
	}

}
